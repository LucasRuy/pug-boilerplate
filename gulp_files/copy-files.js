var gulp = require('gulp');

var images = './app/assets/images/**/*';
var scripts = './app/assets/javascript/**/*.js';

gulp.task('copy:images', function(){
	return gulp.src(images)
	.pipe(gulp.dest('./dist/assets/images'));

});

gulp.task('copy:script', function(){
	return gulp.src(scripts)
	.pipe(gulp.dest('./dist/assets/javascript'));

});

gulp.task('copy', ['copy:images', 'copy:script']);