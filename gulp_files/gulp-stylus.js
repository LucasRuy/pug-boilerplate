var gulp = require('gulp');
var stylus = require('gulp-stylus');

gulp.task('stylus', function() {
	return gulp.src('app/assets/stylesheet/application.styl')
		.pipe(stylus({ compress: true }))
		.pipe(gulp.dest('dist/assets/stylesheet'));

});
