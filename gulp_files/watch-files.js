var gulp = require('gulp');

gulp.task('watch', function(){
	gulp.watch(
		['app/templates/index.pug', 
		 'app/templates/_views/!(_)*.pug',
		 'app/templates/_includes/!(_)*.pug'], ['pug']);
	gulp.watch('app/templates/**/*.pug', ['pug']);
	gulp.watch('app/assets/stylesheet/**/*.styl', ['stylus']);
	gulp.watch('app/assets/javascript/**/*.js', ['copy:script']);
});